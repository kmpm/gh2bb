

module.exports = {
	//do not remove
	'default': {
		defaultKind: 'bug',
		defaultPriority: 'minor',
		defaultAssignee: null,
		defaultComponent: null,
		defaultVersion: null,
		defaultMilestone: null,
		labelToKind: [],
		labelToPriority: [],
		labelToStatus: []
	},

	//TODO: add your own
	// 'sample-repo': {
	// 	defaultKind: 'bug',
	// 	defaultPriority: 'minor',
	// 	labelToKind: [
	// 		{name: 'Error', value: 'bug'},
	// 		{name: 'Features', value: 'enhancement'}
	// 	],
	// 	labelToPriority: [
	// 		{name: 'Priority', value: 'major'}
	// 	],
	// 	labelToStatus: [
	// 		{name: 'More info required', value: 'on hold'}
	// 	]
	// }
};
