/*

To use:
* fill in the user/pass variables at the top
* mode migrate.js
* profit


*/

var GitHubApi = require('github');
var request = require('request');
var child_process = require('child_process');
var async = require('async');
var fs = require('fs-extra');
var path = require('path');
var _ = require('underscore');

// fill in this stuff
var githubUser = '' || process.env.GH_USER;
var githubPass = '' || process.env.GH_PWD;
var githubOrg = '' || process.env.GH_ORG;
var bitbucketUser = '' || process.env.BB_USER;
var bitbucketPass = '' || process.env.BB_PWD;
var bitbucketTeam = '' || process.env.BB_ORG;
var githubBareUser = githubUser.split(':')[0];
var githubToken = '' || process.env.GH_TOKEN;

var checkoutDir = 'git';
var threads = 10;

console.log('GH: %s/%s => BB: %s/%s', githubUser, githubOrg, bitbucketUser, bitbucketTeam);

var github = new GitHubApi({
    version: '3.0.0'
});



async.waterfall([

    // Wipe out previous checkouts
    function(callback) {
        fs.remove(checkoutDir, callback);
    },

    // Make a directory to check out to
    function(callback) {
        fs.ensureDir(checkoutDir, callback);
    },

    // Auth to GitHub
    function(dir, callback) {
        // github.authenticate({
        //     type: 'basic',
        //     username: githubUser,
        //     password: githubPass
        // });
        github.authenticate({
            type: 'oauth',
            token: githubToken
        });
        callback();
    },

    // Get the list of repositories
    function(callback) {
        if (githubOrg && githubOrg.length > 2) {
            github.repos.getFromOrg({
                      org: githubOrg,
                      type: 'all',
                      per_page: 100
                  }, callback);
          }
          else {
            console.info('getting from user');
            github.repos.getAll({
                      type: 'all',
                      per_page: 100
                  }, callback);
          }
    },


], function(error, data) {

    if (error) {
        return console.log('error', error);
    }
    // Ignore public repos, they can stay on GitHub.
    var repos = _.filter(data, function(repo) {
        return repo.private && !repo.fork && (githubOrg ? true : repo.full_name === githubBareUser + '/' +repo.name);
    });
    console.log(repos.length + ' repositories found.');

    async.eachLimit(repos, threads, function(item, callback) {
        var index = repos.indexOf(item) + 1;
        var prompt = index + '/' + repos.length + ' - ' + item.name + ' - ';

        async.waterfall([

            // Clone the repo from GitHub
            function(callback) {
                console.info(prompt + 'cloning from GitHub: %j', item.ssh_url);
                child_process.exec('git clone --mirror ' + item.ssh_url, {
                    cwd: path.join(__dirname, checkoutDir)
                }, function(error, stdout, stderr) {
                    callback(error);
                });
            },

            // Set the new remote
            function(callback) {
                console.info(prompt + 'setting remote ');
                var remote = 'https://' + bitbucketUser + ':' + bitbucketPass + '@bitbucket.org/' + bitbucketTeam + '/' + item.name + '.git';
                child_process.exec('git remote set-url --push origin ' + remote, {
                    cwd: path.join(__dirname, checkoutDir, item.name + '.git')
                }, function(error, stdout, stderr) {
                    callback(error);
                });
            },

            // Delete the Bitbucket repo if it exists.
            function(callback) {
                console.info(prompt + 'deleting Bitbucket repo');
                request
                    .del('https://bitbucket.org/api/2.0/repositories/' + bitbucketTeam + '/' + item.name.toLowerCase(), function(error, response, body) {
                        callback();
                    })
                    .auth(bitbucketUser, bitbucketPass);
            },

            // Create the new Bitbucket repo.
            function(callback) {
                console.info(prompt + 'creating Bitbucket repo');
                request
                    .post('https://bitbucket.org/api/2.0/repositories/' + bitbucketTeam + '/' + item.name.toLowerCase(), function(error, response, body) {
                        body = JSON.parse(body);
                        error = body.error;
                        callback(error);
                    })
                    .form({
                        name: item.name,
                        description: item.description,
                        is_private: true,
                        scm: 'git',
                        has_issues: false,
                        has_wiki: false
                    })
                    .auth(bitbucketUser, bitbucketPass);
            },

            // Push the repo to Bitbucket
            function(callback) {
                console.info(prompt + 'pushing to Bitbucket');
                child_process.exec('git push --mirror', {
                    cwd: path.join(__dirname, checkoutDir, item.name + '.git')
                }, function(error, stdout, stderr) {
                    callback(error);
                });
            },

            // Delete the checkout
            function(callback) {
                console.info(prompt + 'deleting clone');
                fs.remove(path.join(__dirname, checkoutDir, item.name + '.git'), function(error) {
                    callback(error)
                });
            }
        ], function(error, result) {
            // Go on to the next one
            if (error) {
                console.error(prompt + error);
            } else {
                callback();
            }
        });
    });
});