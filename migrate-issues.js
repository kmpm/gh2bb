var GitHubApi = require('github');
var request = require('request');
var child_process = require('child_process');
var async = require('async');
var fs = require('fs-extra');
var path = require('path');
var _ = require('underscore');

var AdmZip = require('adm-zip');

var github = new GitHubApi({
    version: '3.0.0'
});


var githubUser = '' || process.env.GH_USER;
var githubPass = '' || process.env.GH_PWD;
var githubOrg = '' || process.env.GH_ORG;


var githubBareUser = githubUser.split(':')[0];

var githubToken = '' || process.env.GH_TOKEN;


var dumpDir = 'export';

var threads = 10;

var github = new GitHubApi({
    version: '3.0.0'
});


var globalIndex = 0;

var DATE_OF_MARKDOWN = new Date(2012, 9, 4);

/* everything before 4 Oct 2012 needs to be today*/
/*otherwise it will be rendered as creole instead of markdown */
function mdDate(date) {
  d = new Date(date);
  if (d <= DATE_OF_MARKDOWN) {
    return (new Date()).toJSON();
  }
  return d;
}


function githubAuth(callback) {
  console.info('authenticating on github');
  github.authenticate({
            type: 'oauth',
            token: githubToken
        });
  callback();
}

function githubListRepos(callback) {
  console.info('listing github repositories');
  if (githubOrg && githubOrg.length > 2) {
    github.repos.getFromOrg({
              org: githubOrg,
              type: 'all',
              per_page: 100
          }, callback);
  }
  else {
    console.info('getting from user');
    github.repos.getAll({
              type: 'all',
              per_page: 100
          }, callback);
  }
}

var mappings = require('./mappings');

function getMapping(item) {
  if (mappings.hasOwnProperty(item.name)) {
    return mappings[item.name];
  }
  else {
    return mappings.default;
  }
}

function statusMap(item, issue) {
  var map = getMapping(item);
  var value = {
    'open': 'open',
    'closed': 'resolved'
  }[issue.state];

  return valueForLabel(map.labelToStatus, item.labels, value);
}


function kindMap(item, issue) {
  var map = getMapping(item);

  return valueForLabel(map.labelToKind, item.labels, map.defaultKind);
}

function priorityMap(item, issue) {
  var map = getMapping(item);

  return valueForLabel(map.labelToPriority, item.labels, map.defaultPriority);
}

function valueForLabel(list, labels, now) {
  var i, j;
  if (!labels) { return now; }
  value = now;
  for(i = 0; i < list.length; i++) {
    for(j = 0; j < labels.length; j++) {
      if (labels[j].name === list[i].name) {
        value = list[i].value;
        break;
      }
    }
    if (value !== now) {
      break;
    }
  }
  console.log('valueForLabel', now, value);
  return value;
}



/**
 * run this on every repos
 */
function onRepos(item, cb) {
  var index = globalIndex++
  var prompt = index + ' - ' + item.name + ' - ';
  var dir = path.join(dumpDir, item.name);
  fs.ensureDirSync(dir);

  var filename = path.join(dir, 'db-1.0.json');
  var zipname = path.join(dir, item.name + '.zip');
  fs.writeFile(path.join(dir, 'github.json'), JSON.stringify(item, null, '  '));
  var commentIndex = 1;
  var map = getMapping(item);
  var db = {
    meta: {
      default_assignee: map.defaultAssignee,
      default_kind: map.defaultKind,
      default_milestone: map.defaultMilestone,
      default_component: map.defaultComponent,
      default_version: map.defaultVersion
    },
    issues: [],
    comments: []
  };



  async.waterfall([
    githubGetIssues,
    githubToBitbucketIssues,
    function (callback) {
      var data = JSON.stringify(db, null, '  ');

      var zip = new AdmZip();
      zip.addFile('db-1.0.json', new Buffer(data));
      zip.writeZip(zipname);

      fs.writeFile(filename, data, {flag: 'w'}, callback);
    }
  ], function (err, data) {
    if (err) {
      return console.log(prompt + 'err', err);
    }
    console.log(prompt + 'data:', data);
  })


  function githubGetComments(issue) {
    return function (callback) {
      console.info(prompt + 'getting comments for', issue.number);
      github.issues.getComments({
        repo: item.name,
        user: githubOrg || githubBareUser,
        number: issue.number
      }, callback);
    };
  }

  function githubGetIssues(callback) {
    console.info(prompt + 'getting issues');
    github.issues.repoIssues({
      repo: item.name,
      user: githubOrg || githubBareUser,
      state: 'all'
    }, callback);
  }

  function githubToBitbucketIssues(issues, callback) {
    console.log(prompt + 'formatting %s issues', issues.length);

    var bb = [];
    var error = false;
    issues.forEach(function (issue) {
      var i = {
        assignee: issues.asignee,
        created_on: issue.created_at,
        content_updated_on: mdDate(issue.updated_at),
        updated_on: issue.updated_at,
        content: issue.body,
        title: issue.title,
        id: issue.number,
        status: statusMap(item, issue),
        kind: kindMap(item, issue),
        priority: priorityMap(item, issue)
        //gh: issue
      };

      if (['new', 'open', 'resolved', 'on hold', 'invalid', 'duplicate', 'wontfix'].indexOf(i.status) == -1) {
        error = new Error('Bad status `' + i.status + '` in issue ' + issue.number);
      }

      bb.push(i);

    });
    if (error) {
      return callback(error);
    }
    db.issues = db.issues.concat(bb);


    async.waterfall(issues.map(function (issue) {
      if (issue.comments === 0) return function (cb2) { cb2(); };
      return function (cb2) {
        async.waterfall([
          githubGetComments(issue),
          githubToBitbucketComments(issue)
        ], function (err, data) {
          if(err) {
            console.error(prompt, 'error1', err);
          }
          cb2(err);
        })
      };
    }), function (err, data) {
      if (err) {
        console.log(prompt, 'error2', err);
      }
      callback();
    });
  }//-githubToBitbucketIssues


  function githubToBitbucketComments(issue) {
    return function (comments, callback) {
      console.log(prompt + 'formatting %s comments', comments.length);
      var bb = [];
      comments.forEach(function (comment) {
        bb.push({
          id: commentIndex++, //comment.id,
          content: comment.body,
          created_on: comment.created_at,
          updated_on: mdDate(comment.updated_at),
          issue: issue.number,
          user: comment.user.login
          //gh: comment
        });
      });

      db.comments = db.comments.concat(bb);
      callback();
    }
  }

  cb();
}//--onRepos



async.waterfall([
  githubAuth,
  githubListRepos

], function (err, data) {
  if (err) {
    return console.log('err', err);
  }
  // Ignore public repos, they can stay on GitHub.
  var repos = _.filter(data, function(repo) {
    return repo.private && !repo.fork && (githubOrg ? true : repo.full_name === githubBareUser + '/' +repo.name);
  });
  // repos = [repos[0]];

  async.eachLimit(repos, threads, onRepos)

  console.log("done");
});
